# Dreams Home Layouts Plugin

Plugin de Wordpres

## Scripts Disponibles

En el directorio del proyecto, para descargar las dependencias:

### `npm install`

Ejecuta la aplicacion en modo desarrollo.<br />

### `npm run dev`

Ejecuta la aplicacion para modo de produccion.<br />

### `npm run build`



## Consideraciones del plugin

Para que el plugin funcione es requerido tener instaladados los siguientes plugins:

- [Metabox io](https://metabox.io/ "Metabox io")
- [Post Connector](https://wordpress.org/plugins/post-connector/ "Post Connector")


## Ramas

- *master* Rama que se usa para produccion
- *f-dev* Rama de desarrollo







