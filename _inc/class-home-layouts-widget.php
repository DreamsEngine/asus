<?php


class HomeLayoutsRegisterWidget {

    public function initialize() {
        add_action('widgets_init', array($this, 'register'));
    }

     function register() {
        register_widget('HomeLayoutsWidget');
    }
}

class HomeLayoutsWidget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'pinno_home_layouts_widget',
            'description' => 'A widget that displays the layouts posts',
        );
        parent::__construct( 'pinno_home_layouts_widget', '✳️ ITL - Home Layouts Widget', $widget_ops);
    }

    public function widget($args, $instance) {
        include(plugin_dir_path(__FILE__) . 'views/widget-home-layouts-site-template.php');
    }

    public function form($instance) {
        include(plugin_dir_path(__FILE__) . 'views/widget-home-layouts-admin-template.php');
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        return $instance;
    }

}
