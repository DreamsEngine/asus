<?php


class HomeWidgetAjaxEnpoints
{

    public function initialize()
    {
        add_action( 'wp_ajax_get_post_layouts_home', array($this, 'get_post_layouts_home'));
        add_action( 'wp_ajax_delete_post_layouts_home', array($this, 'delete_post_layouts_home'));
    }

    public function get_post_layouts_home()
    {
        $post_manager = new HomeSticky();

        $post_1 = $post_manager->get_post_by_position('position-1');
        $post_2 = $post_manager->get_post_by_position('position-2');
        $post_3 = $post_manager->get_post_by_position('position-3');
        $post_4 = $post_manager->get_post_by_position('position-4');
        $post_5 = $post_manager->get_post_by_position('position-5');

        $callback_map = function($post_item) {
            $post_item->thumbnail_url = get_the_post_thumbnail_url($post_item->ID);
            return $post_item;
        };

        $post_1 = array_map($callback_map, $post_1);
        $post_2 = array_map($callback_map, $post_2);
        $post_3 = array_map($callback_map, $post_3);
        $post_4 = array_map($callback_map, $post_4);
        $post_5 = array_map($callback_map, $post_5);

        $options = array($post_1, $post_2, $post_3, $post_4, $post_5);


        echo json_encode($options);
        wp_die();
    }


    public function delete_post_layouts_home()
    {
        global $prefix;

        if(isset($_POST['ids'])) {
            $list_ids = explode(',', $_POST['ids']);

            foreach ($list_ids as $post_id) {
                delete_post_meta($post_id, $prefix . 'position-layout');
            }

            echo json_encode(array('status' => 'ok', 'ids' => $list_ids));
        } else {
            echo json_encode(array('status' => 'error', 'message' => 'missings ids'));
        }

        wp_die();
    }

}
