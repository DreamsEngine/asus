<?php


class HomeEnqueue {

    private static $version_assets = '0.0.61';

    public function initialize_enqueue_scripts_admin() {
        add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts_admin'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_styles_admin'));
    }

    public function initialize_enqueue_scripts_site() {
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_site'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_styles_site'));
    }

    public function enqueue_scripts_admin() {
        $screen = get_current_screen();

        if ($screen->id == 'post' || $screen->id == 'edit-post' || $screen->id == 'toplevel_page_home-layouts-list-home') {
            wp_enqueue_script('dreams-home-js-admin', plugin_dir_url( __FILE__ ) . '/assets/js/admin.js', array(), self::$version_assets);

            wp_localize_script('dreams-home-js-admin', 'wp_ajax_home_layout', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
        }
    }

    public function enqueue_styles_admin() {
        $screen = get_current_screen();
        if ($screen->id == 'post' || $screen->id == 'toplevel_page_home-layouts-list-home') {
            wp_enqueue_style('dreams-home-css-admin', plugin_dir_url( __FILE__ ) . '/assets/css/admin.css', array(), self::$version_assets);
        }

    }

    public function enqueue_scripts_site() {
        wp_enqueue_script('dreams-home-js-site', plugin_dir_url( __FILE__ ) . '/assets/js/site.js', array(), self::$version_assets, false);
    }

    public function enqueue_styles_site() {
        wp_enqueue_style('dreams-home-css-site', plugin_dir_url( __FILE__ ) . '/assets/css/site.css', array(), self::$version_assets);
    }
}