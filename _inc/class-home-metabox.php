<?php


class HomeMetabox {

    public function initialize() {
        add_filter('rwmb_meta_boxes', array($this, 'register'));
    }

    function register($metaBoxes) {

        global $prefix;
        $base_url = plugin_dir_url( __FILE__ );

        /*$metaBoxes[] = array(
            'title'      => esc_html__( 'Layout', 'dreamstheme' ),
            'id'         => $prefix . 'select-layout',
            'post_types' => array( 'post'),
            'autosave'   => true,
            'fields'     => array(
                array(
                    'id'               => $prefix . 'home-layout',
                    'name'             => esc_html__( 'Layout', 'dreamstheme' ),
                    'type'             => 'image_select',
                    'max_file_uploads' => '4',
                    'options'          => array(
                        'front-0' => $base_url . 'assets/img/front-0.jpg',
                        'front-1' => $base_url . 'assets/img/front-1.jpg',
                        'front-2' => $base_url . 'assets/img/front-2.jpg',
                        'front-3' => $base_url . 'assets/img/front-3.jpg'
                    ),
                    'std' => 'front-0',
                ),

                array(
                    'id'      => $prefix . 'layout-video-url',
                    'name'    => esc_html__( 'URL del video', 'dreamstheme' ),
                    'type'    => 'oembed',
                    'title'   => esc_html__( 'Seleccióna la URL del video', 'dreamstheme' ),
                    'visible' => [ $prefix . 'home-layout', '=', 'front-3' ]

                ),
                array(
                    'id'               => $prefix . 'layout-video-logo',
                    'name'             => esc_html__( 'Imagen de los logos (384px  x  216px)', 'dreamstheme' ),
                    'type'             => 'image_advanced',
                    'title'            => esc_html__( 'Seleccióna  la imagen de los logos', 'dreamstheme' ),
                    'max_file_uploads' => 1,
                    'visible'          => [ $prefix . 'home-layout', '=', 'front-3' ]
                ),
                array(
                    'id'               => $prefix . 'layout-video-background',
                    'name'             => esc_html__( 'Imagen de fondo', 'dreamstheme' ),
                    'type'             => 'image_advanced',
                    'title'            => esc_html__( 'Seleccióna la imagen de fondo', 'dreamstheme' ),
                    'max_file_uploads' => 1,
                    'visible'          => [ $prefix . 'home-layout', '=', 'front-3' ]

                ),
                array(
                    'id'      => $prefix . 'layout-fixed-background',
                    'name'    => esc_html__( 'Posición de la imagen de fondo', 'dreamstheme' ),
                    'type'    => 'radio',
                    'title'   => esc_html__( 'Seleccióna la imagen de fondo', 'dreamstheme' ),
                    'options' => array(
                        'nofixed' => 'No Fijo',
                        'fixed'   => 'Fijo'
                    ),
                    'visible' => [ $prefix . 'home-layout', '=', 'front-3' ]
                ),
                array(
                    'id'      => $prefix . 'layout-links-go',
                    'name'    => esc_html__( 'Link general de pauta ', 'dreamstheme' ),
                    'type'    => 'url',
                    'title'   => esc_html__( 'Escribe el link al que va dirgida esta pauta', 'dreamstheme' ),
                    'visible' => [ $prefix . 'home-layout', '=', 'front-3' ]
                )
            )
        );*/


        $metaBoxes[] = array(
            'title'      => esc_html__( 'Posición', 'dreamstheme' ),
            'id'         => $prefix . 'position-articles',
            'post_types' => array( 'post'),
            'autosave'   => true,
            'context' => 'advanced',
            'fields'     => array(
                array(
                    'id'               => $prefix . 'position-layout',
                    'name'             => esc_html__( 'Posición', 'dreamstheme' ),
                    'type'             => 'image_select',
                    'max_file_uploads' => '6',
                    'options'          => array(
                        'position-0' => $base_url . 'assets/img/position-0.jpg',
                        'position-1' => $base_url . 'assets/img/position-1.jpg',
                        'position-2' => $base_url . 'assets/img/position-2.jpg',
                        'position-3' => $base_url . 'assets/img/position-3.jpg',
                        'position-4' => $base_url . 'assets/img/position-4.jpg'//,
                        //'position-5' => $base_url . 'assets/img/position-5.jpg'
                    ),
                    'std' => 'position-0',
                    'visible' => [ $prefix . 'sidebarCheck', '=', false ]
                ),
            )
        );


        return $metaBoxes;
    }
}