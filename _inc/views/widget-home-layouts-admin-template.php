<?php

$metabox_plugin_exist = function_exists('rwmb_meta');
$post_connector_plugin_exist = class_exists('Post_Connector');

$metabox_plugin_icon =  $metabox_plugin_exist ? '✅' : '❌';
$metabox_plugin_text = $metabox_plugin_exist ? 'Metabox plugin installed' : 'Missing install Metabox plugin';

$post_connector_plugin_icon =  $post_connector_plugin_exist ? '✅' : '❌';
$post_connector_plugin_text = $post_connector_plugin_exist ? 'Post Connector plugin installed' : 'Missing install Post Connector plugin';

?>

<ul>
    <li><?php echo "$metabox_plugin_icon $metabox_plugin_text" ?></li>
    <li><?php echo "$post_connector_plugin_icon $post_connector_plugin_text" ?></li>
</ul>


