<?php

$layout_info = HomeQueryPost::get_sticky_post_info();
if($layout_info === null) return;

if($layout_info->layout_type == HomeQueryPost::KEY_FRONT_1): ?>
<section class="front-1">
    <div class="pinno-main-box">
        <article class="front-1__article">
            <figure class="front-1__figure">
                <a href="<?php echo $layout_info->main_post->post_link; ?>">
                    <?php echo $layout_info->main_post->post_thumbnail; ?>
                </a>
            </figure>
            <aside class="front-1__aside">
                <a href="<?php echo $layout_info->main_post->post_link; ?>">
                    <h3 class="front-1__title"><?php echo $layout_info->main_post->post_title; ?></h3>
                </a>

                <p class="front-1__excertp"><?php echo $layout_info->main_post->post_excerpt; ?></p>

                <div class="front-1__meta">
                    <p>por <?php echo $layout_info->main_post->post_author_name; ?> | <?php echo $layout_info->main_post->post_date_format; ?> </p>
                    <p>en
                        <a href="<?php echo $layout_info->main_post->post_category_link; ?>">
                            <?php echo $layout_info->main_post->post_category_name; ?>
                        </a>
                    </p>
                </div>

                <p class="front-1__more">Te puede intesar</p>

                <ul class="front-1__list">
                    <?php foreach ($layout_info->aside_posts as $related): ?>
                        <li class="front-1__list__item">
                            <a href="<?php echo $related->post_link; ?>">
                                <?php echo $related->post_title; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </aside>
        </article>
    </div>
</section>
<?php elseif ($layout_info->layout_type == HomeQueryPost::KEY_FRONT_2): ?>

<section class="front-2">
    <div class="pinno-main-box">
        <article class="front-2__article">
            <figure class="front-2__figure">
                <a href="<?php echo $layout_info->main_post->post_link; ?>">
                    <?php echo $layout_info->main_post->post_thumbnail; ?>
                </a>
            </figure>
            <div class="front-2__meta">
                <h2 class="front-2__title">
                    <a href="<?php echo $layout_info->main_post->post_link; ?>">
                        <?php echo $layout_info->main_post->post_title; ?>
                    </a>
                </h2>

                <p class="front-2__author">por <?php echo $layout_info->main_post->post_author_name; ?> </p>
                <p class="front-2__date"><?php echo $layout_info->main_post->post_date_format; ?> </p>
                <p class="front-2__category">En
                    <a href="<?php echo $layout_info->main_post->post_category_link; ?>">
                        <?php echo $layout_info->main_post->post_category_name; ?>
                    </a>
                </p>
            </div>
        </article>
        <?php foreach ($layout_info->aside_posts as $related): ?>
            <article class="front-2__article">
                <figure class="front-2__figure">
                    <a href="<?php echo $related->post_link; ?>">
                        <?php echo $related->post_thumbnail; ?>
                    </a>
                </figure>
                <div class="front-2__meta">
                    <h2 class="front-2__title">
                        <a href="<?php echo $related->post_link; ?>">
                            <?php echo $related->post_title; ?>
                        </a>
                    </h2>

                    <p class="front-2__author">por <?php echo $related->post_author_name; ?> </p>
                    <p class="front-2__date"><?php echo $related->post_date_format; ?> </p>
                    <p class="front-2__category">En
                        <a href="<?php echo $related->post_category_link; ?>">
                            <?php echo $related->post_category_name; ?>
                        </a>
                    </p>
                </div>
            </article>
        <?php endforeach; ?>
    </div>
</section>

<?php elseif ($layout_info->layout_type == HomeQueryPost::KEY_FRONT_3): ?>
<section class="front-3 <?php echo $layout_info->main_post->post_video_fixed_background?>" <?php echo $layout_info->main_post->post_style_background; ?> >
    <div class="pinno-main-box">
        <laoctava-front-3
                iframe='<?php echo $layout_info->main_post->post_video_iframe; ?>'
                logo="<?php echo $layout_info->main_post->post_video_logo; ?>"
                link="<?php echo $layout_info->main_post->post_video_links_go; ?>"
                thumbnail="<?php echo $layout_info->main_post->post_thumbnail_url; ?>"
                iconplay="<?php echo $layout_info->main_post->post_play_url; ?>"
        ></laoctava-front-3>
    </div>
</section>
<?php endif; ?>


