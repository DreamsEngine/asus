<?php
 $posts_feat = HomeQueryPost::get_feat_posts(HomeWidgetLayoutsConfig::get_number_post());
?>


<?php 


?>

<section id="pinno_home" class="pinno-widget-home left relative">
    <div class="pinno-main-box pinno-main-box-full-">
        <div class="channel__content" >
            <!-- Cards -->
           
            <!--=================== AGUS VERSION =========================== -->

      <div id="fullwidth-wrap" class="left relative">
    
      <a href="<?php echo $posts_feat[0]->post_link; ?>" aria-label="<?php echo $posts_feat[0]->post_title; ?>" data-ga-track="Cover Story - blog - Position <?php echo $posts_feat[0]->post_position; ?> - <?php echo $posts_feat[0]->post_title; ?>">
      <div id="fullwidth_top" class="left relative">
        <div id="pinno-feat6-img" class="right relative">
          <img src="<?php echo $posts_feat[0]->post_thumbnail_url; ?>" class="show-img show-img-special" enhanced="<?php echo $posts_feat[0]->post_thumbnail_url; ?>">
        </div><!--pinno-feat6-img-->
        <div id="pinno-feat6-text">
          <div class="feat_text_holder">
            <h3 class="pinno-feat1-pop-head"><span class="pinno-feat1-pop-head"><?php echo $posts_feat[0]->post_category_name; ?> </span></h3>
            <h2><span><?php echo $posts_feat[0]->post_title; ?></span></h2>
            <p><?php echo $posts_feat[0]->post_excerpt; ?></p>
          </div>
        </div><!--pinno-feat6-text-->
      </div><!--pinno-feat6-main-->
      </a>
  </div><!--pinno-feat6-wrap-->

  <div class="feat_holder left relative">
  <?php array_shift($posts_feat); ?>
  <?php $option_tags = HomeWidgetLayoutsConfig::get_options_tags(); ?>
  <?php foreach ($posts_feat as $post_item): ?>

    <a href="<?php echo $post_item->post_link; ?>" rel="bookmark" class="feat_block" data-ga-track="Homepage <?php echo $post_item->post_category_name; ?> - <?php echo $post_item->post_title; ?>">
    <div class="pinno-widget-feat1-bot-story left relative">
        <div class="pinno-widget-feat1-bot-img left relative">
            <img width="400" height="240" src="<?php echo $post_item->post_thumbnail_url; ?>" class="pinno-reg-img lazy wp-post-image" alt="" loading="lazy" >
            <img width="80" height="80" src="<?php echo $post_item->post_thumbnail_url; ?>" class="pinno-mob-img lazy wp-post-image" alt="" loading="lazy" srcset="<?php echo $post_item->post_thumbnail_url; ?>" sizes="(max-width: 80px) 100vw, 80px">
        </div><!--pinno-widget-feat1-bot-img-->
        <div class="pinno-widget-feat1-bot-text left relative">
                      <div class="pinno-cat-date-wrap left relative">
                        <span class="pinno-cd-cat left relative"><?php echo $post_item->post_category_name; ?></span>
                        <span class="pinno-cd-date left relative" style="display:none;">2 days ago</span>
                      </div><!--pinno-cat-date-wrap-->
                      <h2><?php echo $post_item->post_title; ?></h2>
                      <p><?php echo $post_item->post_excerpt; ?></p>
                    </div><!--pinno-widget-feat1-bot-text-->
                  </div><!--pinno-widget-feat1-bot-story-->
    </a>

  <?php endforeach; ?>

  </div>

            <!--=================== AGUS VERSION ENDS =========================== -->

            <!-- End Cards -->

            <!-- Aside //-->
            <div class="channel__sidebar channel__sidebar--pop-picks pop-picks pop-picks--pop-active" style="display: none !important;">
                <div class="pop-picks__nav">

                    <button id="pick-popular" class="pop-picks__nav-btn pop-picks__nav-btn--popular pop-picks__nav-btn--active">
                        <span>Últimas noticias</span>
                        <div class="pop-picks__flipper">
                            <div class="pop-picks__square pop-picks__square--left"></div>
                            <div class="pop-picks__square pop-picks__square--right"></div>
                        </div>
                    </button>

                    <button id="pick-editors" class="pop-picks__nav-btn pop-picks__nav-btn--ep">
                        <span> Editors' Picks </span>
                        <div class="pop-picks__flipper">
                            <div class="pop-picks__square pop-picks__square--left"></div>
                            <div class="pop-picks__square pop-picks__square--right"></div>
                        </div>
                    </button>



                </div>
                 <!-- pop-picks__nav //-->



                <div id="popular-tab" class="pick-tab show">
                    <laoctava-most-popular></laoctava-most-popular>
                </div> <!-- pick-tab //-->

                <div id="editors-tab" class="pick-tab">
                    <?php
                    //Loop Arguments
                    $module_two_last_args = array(

                        'no_found_rows' => true,
                        'update_post_meta_cache' => false,
                        'update_post_term_cache' => false,
                        'category_name' => "editors-picks",
                        'posts_per_page' => 5,
                        'post_type' => 'post',
                        'ignore_sticky_posts' => true,
                        'post-type' => 'any'

                    );

                    //Query
                    $module_two_last = new WP_Query($module_two_last_args); ?>

                    <?php
                    //Begin Loop
                    if ($module_two_last->have_posts()) :
                        while ($module_two_last->have_posts()) :
                            $module_two_last->the_post(); ?>

                            <article class="la8_module--two__aside__article">
                                <figure class="<?php tagging_banner(); ?>">
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_post_thumbnail('full', array('class' => 'la8_module-image--full lazyload', 'data-object-fit' => 'cover')); ?>
                                    </a>
                                </figure>
                                <div class="la8_module--two__aside__article__metas">
                                    <h6 class="la8_module--two__aside__article__title la8_title">
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h6>
                                    <time class="la8_module--two__aside__article__time"><?php echo get_the_date(); ?></time>
                                </div>
                            </article>

                        <?php
                        endwhile;
                    endif;
                    //End Loop
                    wp_reset_postdata(); ?>

                </div> <!-- pick-tab //-->
                <!-- End Last -->



                <div class="pinno-widget-ad left relative" style="margin:0.75em auto 0;">
                    <span class="pinno-ad-label">Publicidad</span>
                    <div class="pinno-ad-serving" data-post-id="256601">
                        <div id="div-gpt-ad-la8-default-box-01-256601"></div>
                    </div>
                </div>

            </div>
            <!-- End Aside -->

        </div>
</section> <!-- module-2 //-->