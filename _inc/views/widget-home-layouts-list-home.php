<h1>Entradas Fijas en el Home</h1>

<section id="home-layouts-list-wrapper">

    <div class="home-layouts-list">
        <div class="home-layouts-item home-layouts-item--one" id="layout-position-1">
            <div class="home-layouts-item__img">
                <span>1</span>
            </div>
            <div class="home-layouts-item__info">

            </div>
        </div>
        <div class="home-layouts-item home-layouts-item--two" id="layout-position-2">
            <div class="home-layouts-item__img">
                <span>2</span>
            </div>
            <div class="home-layouts-item__info">

            </div>
        </div>
        <div class="home-layouts-item home-layouts-item--three" id="layout-position-3">
            <div class="home-layouts-item__img">
                <span>3</span>
            </div>
            <div class="home-layouts-item__info">

            </div>
        </div>
        <div class="home-layouts-item home-layouts-item--four" id="layout-position-4">
            <div class="home-layouts-item__img">
                <span>4</span>
            </div>
            <div class="home-layouts-item__info">

            </div>
        </div>
        <div class="home-layouts-item home-layouts-item--five" id="layout-position-5">
            <div class="home-layouts-item__img">
                <span>5</span>
            </div>
            <div class="home-layouts-item__info">

            </div>
        </div>
    </div>

</section>