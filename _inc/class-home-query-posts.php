<?php

class HomeQueryPost {

    const KEY_MAIN_POST = 'DREAMS_MAIN_LAYOUT_POST';
    const KEY_SLUG_POST_CONECTOR = 'layouts-related-posts';

    const KEY_FRONT_1 = 'front-1';
    const KEY_FRONT_2 = 'front-2';
    const KEY_FRONT_3 = 'front-3';

    /***
     * @return HomeLayoutsModel|null
     */
    public static function get_sticky_post_info() {

        $metabox_plugin_exist = function_exists('rwmb_meta');
        $post_connector_plugin_exist = class_exists('Post_Connector');


        if($metabox_plugin_exist && $post_connector_plugin_exist) {

            $sticky_posts_ids = get_option('sticky_posts');

            if(is_array($sticky_posts_ids) && count($sticky_posts_ids) > 0) {


                $args = array(
                    'no_found_rows' => true,
                    'update_post_meta_cache' => false,
                    'update_post_term_cache' => false,

                    'posts_per_page' => 1,
                    'post_type' => 'any',
                    'post__in' => $sticky_posts_ids,
                );

                $posts_list = get_posts($args);

                if(count($posts_list) > 0) {

                    global $prefix;

                    $post = $posts_list[0];
                    $layout = rwmb_meta($prefix . 'home-layout', '', $post->ID);
                    $related_list = [];

                    if($layout == self::KEY_FRONT_1 || $layout == self::KEY_FRONT_2) {
                        $manager = new SP_Post_Link_Manager();
                        $related_list = $manager->get_children(self::KEY_SLUG_POST_CONECTOR, $post->ID);

                        $post->post_thumbnail = get_the_post_thumbnail($post->ID, 'full', array('class' => 'la8_module-image--full lazyload', 'data-object-fit' => 'cover'));
                        $post->post_link = get_permalink($post->ID);
                        $post->post_author_name = get_the_author_meta('display_name', $post->post_author);
                        $post->post_date_format = sprintf(esc_html__( '%s ago', 'iggy-type-0' ),human_time_diff(strtotime($post->post_date), current_time( 'timestamp')));

                        $categories = get_the_category($post->ID);
                        $post->post_category_name = (count($categories) > 0) ? get_cat_name($categories[0]->term_id) : '';
                        $post->post_category_link = (count($categories) > 0) ? get_category_link($categories[0]->term_id) : '';
                    }

                    if($layout == self::KEY_FRONT_3) {

                        $post->post_video_iframe = rwmb_meta($prefix . 'layout-video-url', '', $post->ID);

                        $logo_meta = rwmb_meta($prefix . 'layout-video-logo', array( 'limit' => 1 ), $post->ID);
                        $post->post_video_logo = count($logo_meta) > 0 ? $logo_meta[0]["full_url"] : '';

                        $background_meta = rwmb_meta($prefix . 'layout-video-background', array( 'limit' => 1 ), $post->ID);
                        $post->post_video_background = count($background_meta) > 0 ? $background_meta[0]["full_url"] : '';
                        $post->post_style_background = $post->post_video_background !== '' ? "style='background-image: url($post->post_video_background)'" : '';

                        $post->post_video_fixed_background = rwmb_meta($prefix . 'layout-fixed-background', '', $post->ID);
                        $post->post_video_links_go = rwmb_meta($prefix . 'layout-links-go', '', $post->ID);

                        $post->post_thumbnail_url = get_the_post_thumbnail_url($post->ID);
                        $post->post_play_url = plugin_dir_url( __FILE__ ) . 'assets/img/play.svg';
                    }

                    if(is_array($related_list) && $layout != self::KEY_FRONT_3) {
                        foreach ($related_list as $related) {

                            $related->post_link = get_permalink($related->ID);

                            if($layout == self::KEY_FRONT_2) {
                                $related->post_thumbnail = get_the_post_thumbnail($related->ID, 'full', array('class' => 'la8_module-image--full lazyload', 'data-object-fit' => 'cover'));

                                $related->post_author_name = get_the_author_meta('display_name', $related->post_author);
                                $related->post_date_format = sprintf(esc_html__( '%s ago', 'iggy-type-0' ),human_time_diff(strtotime($related->post_date), current_time( 'timestamp')));

                                $categories_related = get_the_category($post->ID);
                                $related->post_category_name = (count($categories_related) > 0) ? get_cat_name($categories_related[0]->term_id) : '';
                                $related->post_category_link = (count($categories_related) > 0) ? get_category_link($categories_related[0]->term_id) : '';
                            }
                        }
                    }

                    $home_layout = new HomeLayoutsModel();
                    $home_layout->main_post = $post;
                    $home_layout->layout_type = $layout;
                    $home_layout->aside_posts = $layout == self::KEY_FRONT_2 ? array_slice($related_list, 0, 2): $related_list;

                    $GLOBALS[self::KEY_MAIN_POST] = $post;

                    return $home_layout;
                }
            }

        }

        return null;
    }


    public static function get_feat_posts($number_post_options) {

        $post_one = self::get_post_by_meta_key_with_exclude('position-1');
        $post_two = self::get_post_by_meta_key_with_exclude('position-2');
        $post_three = self::get_post_by_meta_key_with_exclude('position-3');
        $post_four = self::get_post_by_meta_key_with_exclude('position-4');
        //$post_five = self::get_post_by_meta_key_with_exclude('position-5');

    $post_list = array($post_one, $post_two, $post_three, $post_four/*, $post_five*/);

        $count_post_list = count($post_list);

        if($number_post_options > 3) {
            for ($i = $count_post_list; $i < $number_post_options; $i++) { 
               array_push($post_list, null);
            }
        }

        $not_null_callable = function($accumulate, $item) {
            return $accumulate += $item == null ? 0 : 1;
        };

        $post_not_null_count = array_reduce($post_list, $not_null_callable, 0);
        
        $posts_missing_count = $number_post_options - $post_not_null_count;
        if($posts_missing_count == 0) {
            return self::populate_feat_posts($post_list);
        } else {
            $posts_remaining_list = self::get_feat_post_per_page_with_exclude($posts_missing_count, $post_list);
            $post_merge = self::get_merge_posts($post_list, $posts_remaining_list);
            return self::populate_feat_posts($post_merge);
        }
    }

    public static function get_post_by_meta_key_with_exclude($key) {

        global $prefix;
        $first_layout_post = isset($GLOBALS[self::KEY_MAIN_POST]) ? $GLOBALS[self::KEY_MAIN_POST] : null;

        $args = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,

            'posts_per_page' => 1,
            'post_type' => 'post',
            'meta_key' => $prefix . 'position-layout',
            'meta_value' => $key
        );

        if($first_layout_post != null) {
            $args['post__not_in'] = array($first_layout_post->ID);
        }

        $posts_list = get_posts($args);
        return count($posts_list) > 0 ? $posts_list[0] : null;
    }

    public static function get_feat_post_per_page_with_exclude($per_page, $post_list) {

        $first_layout_post = isset($GLOBALS[self::KEY_MAIN_POST]) ? $GLOBALS[self::KEY_MAIN_POST] : null;

        $filter_posts = array_filter($post_list, function ($item){ return $item != null; });
        $exclude_posts = array_map(function ($item) { return $item->ID; }, $filter_posts);

        

        $args = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'posts_per_page' => $per_page,
            'ignore_sticky_posts' => true,
            'post_type' => array('post'),
            'post__not_in' => $exclude_posts,
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => 'den_sidebarCheck',
                    'compare' => 'NOT EXISTS'
                ),
                array(
                    'key' => 'den_sidebarCheck',
                    'compare' => '!=',
                    'value' => '1'
                ),
            )
        );

        if($first_layout_post != null) {
            array_push($args['post__not_in'], $first_layout_post->ID);
        }

        return get_posts($args);
    }

    private static function get_merge_posts($position_list, $remaining_list) {

        $remaining_index = 0;
        $map_callable = function($item) use ($remaining_list, &$remaining_index) {

            if($item != null) {
                return $item;
            }

            if(isset($remaining_list[$remaining_index])) {
                $item = $remaining_list[$remaining_index];
                $remaining_index++;
            }

            return $item;
        };

        return array_map($map_callable, $position_list);
    }

    private static function populate_feat_posts($position_list) {

        $map_callable = function ($item, $index) {

            $item->post_position = $index;
            $item->post_link = get_permalink($item->ID);
            $item->post_thumbnail_url = get_the_post_thumbnail_url($item->ID, 'full');

            $categories = get_the_category($item->ID);
            $item->post_category_name = (count($categories) > 0) ? get_cat_name($categories[0]->term_id) : '';
            $item->post_category_link = (count($categories) > 0) ? get_category_link($categories[0]->term_id) : '';

            $item->post_excerpt = wp_trim_words(get_the_excerpt($item->ID), 20, '...' );

            if($index == 0) {
                $item->post_author_name = get_the_author_meta('display_name', $item->post_author);
                $item->post_author_link = get_the_author_meta('user_url', $item->post_author);
            }

            return $item;
        };

        return array_map($map_callable, $position_list, array_keys($position_list));
    }
}