document.addEventListener('DOMContentLoaded', function() {

    var isActiveGutenberg = wp !== 'undefined' && typeof wp.blocks !== 'undefined';
    var layouts = document.getElementsByName('den_home-layout');
    var numbers = document.getElementsByName('den_position-layout');

    if(isActiveGutenberg) {

        layouts.forEach(function(check) {
            check.addEventListener('change', function(){

                var labelSticky = document.querySelector('[for="inspector-checkbox-control-0"]');
                var stickyInput = document.getElementById('inspector-checkbox-control-0');

                if(labelSticky && stickyInput) {
                    var layoutType = this.value;

                    if(layoutType === 'front-0') {
                        if(stickyInput.checked) {
                            labelSticky.click();
                        }
                    } else {
                        if(!stickyInput.checked) {
                            labelSticky.click();
                        }
                    }
                }

            });
        });

        numbers.forEach(function(numberCheck) {
            var container = document.getElementById('den_noDest');

            var hideOrShowMetabox = function(numberCheck) {
                if(container) {
                    if(numberCheck.value == 'position-0') {
                        container.style.display = 'block';
                    } else {
                        container.style.display = 'none';
                    }
                }
            }

            numberCheck.addEventListener('change', function(){
                hideOrShowMetabox(numberCheck);
            });

            if(numberCheck.checked) {
                hideOrShowMetabox(numberCheck);
                return;
            }

        });
        
    } else {

        var stiky = document.getElementById('sticky');

        layouts.forEach(function(check) {
            check.addEventListener('change', function(){
                if(stiky != null) {
                    stiky.checked = this.value !== 'front-0';
                }
            });
        });
    }


    
    if(window.location.href.indexOf("edit.php") > 0) {
        
        var titlesList = document.querySelectorAll(".row-title");
        titlesList.forEach(function(titleItem){
            var content = titleItem.innerHTML;
            if(content.indexOf("— Entrada Home #") > 0) {
                var split = content.split("—");
                titleItem.innerHTML = split[0] + " " + "<b style='color: #555;'> — 🏠 " + split[1] + "</b>";
            }
        });
    }

    if(window.location.href.indexOf("home-layouts-list-home") > 0) {
        console.log("ready!!!!")
        fetchWpAdmin("get_post_layouts_home")
            .then(function(data) {
                if(Array.isArray(data)) {
                    data.forEach(function(items, index) {
                        if(items.length > 0) {
                            var position = index + 1;
                            var postElement = document.getElementById("layout-position-" + position);

                            var post = items[0];

                            var imgContainer = postElement.querySelector(".home-layouts-item__img");
                            imgContainer.innerHTML = "";
                            var imgTag = document.createElement("img");
                            imgTag.src = post.thumbnail_url;
                            imgContainer.appendChild(imgTag);

                            var infoContainer = postElement.querySelector(".home-layouts-item__info");

                            var titleElement = document.createElement("span");
                            titleElement.innerHTML = post.post_title;

                            var deleteButton = document.createElement("button");
                            deleteButton.innerHTML = "Quitar";
                            deleteButton.className = "home-layout-btn-action";
                            deleteButton.type = "button";

                            infoContainer.appendChild(titleElement);
                            infoContainer.appendChild(deleteButton);

                            deleteButton.addEventListener('click', function() {
                                this.disabled = true;
                                var ids = items.map(function(postItem){ return postItem.ID });

                                fetchWpAdmin("delete_post_layouts_home", "ids=" + ids.toString())
                                    .then(function(response){
                                        if(response.status === 'ok') {
                                            alert('Se quito del home la entrada');
                                            window.location.reload();
                                        } else {
                                            alert('Ocurrio un error al borrar');
                                        }
                                });
                            })
                        }
                    });
                }
            })
    }



    function fetchWpAdmin(action, params) {
        return fetch(wp_ajax_home_layout.ajaxurl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
            body: params ? "action=" + action + "&" + params : "action=" + action,
            credentials: 'same-origin'
        }).then(function(res) { return res.json(); });
    }

});


