<?php


class HomeLayoutsListHome
{

    public function initialize()
    {
        add_action('admin_menu', array($this, 'my_admin_menu'));
    }


    function my_admin_menu() {
        add_menu_page( 'Home Layouts Posts Fixed List', 'Entradas Fijas', 'manage_options', 'home-layouts-list-home', array($this, 'myplguin_admin_page'), 'dashicons-awards
', 99);

    }

    function myplguin_admin_page(){
        require_once(
            plugin_dir_path(__FILE__) . 'views/widget-home-layouts-list-home.php'
        );
    }


}
