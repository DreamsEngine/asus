<?php

class HomeWidgetLayoutsConfig {

	const NUMBER_POST_KEY = 'home-widget-layout-custom_number_post';
	const TAG_SPECIAL_KEY_1 = 'home-widget-layout-custom_tag_special_1';
	const TAG_SPECIAL_KEY_2 = 'home-widget-layout-custom_tag_special_2';

	public function initialize() {

		add_action('admin_menu', array($this, 'menu_callback'));

		if (is_admin()) {
			add_action('admin_init', array($this, 'register_options'));
		}
	}

	public function menu_callback() {
		add_options_page('Home Layouts Config Page', 'Home Layouts Config', 'manage_options', 'home-widget-layouts-options', array($this, 'menu_markup') );
	}

	public function menu_markup() {
		if (!current_user_can( 'manage_options'))  {
			wp_die( __('You do not have sufficient permissions to access this page.'));
		}

		$tags_list = get_tags(array('hide_empty' => false));
		$number_posts = esc_attr(get_option(self::NUMBER_POST_KEY));


		$tag_1 = esc_attr(get_option(self::TAG_SPECIAL_KEY_1));
		$tag_2 = esc_attr(get_option(self::TAG_SPECIAL_KEY_2));
	?>
		<div class="wrap">
			<h1>Plugin Options</h1>
			<form method="post" action="options.php"> 
				<?php settings_fields( 'home-widget-layout-custom-group' ); ?>
				<?php do_settings_sections('home-widget-layout-custom-group'); ?>

				<table class="form-table">
			        <tr valign="top">
			        	<th scope="row">Numero de post en el home (Predeterminado 5)</th>
			        	<td><input class="regular-text" type="number" min="5" max="11" step="2" name="<?php echo self::NUMBER_POST_KEY; ?>" value="<?php echo $number_posts; ?>" /></td>
			        </tr>
					<tr valign="top">
			        	<th scope="row">Tag 1 especial en el home</th>
			        	<td>
							<select 
								name="<?php echo self::TAG_SPECIAL_KEY_1; ?>" 
								id="<?php echo self::TAG_SPECIAL_KEY_1; ?>" 
								class="regular-text">

								<option value="" <?php echo $tag_1 == "" ? "selected" : "" ?>>Selectione un tag</option>
								<?php foreach($tags_list as $tag): ?>
									<?php $slug = $tag->slug; ?>
									<?php $selected = $tag_1 == $tag->slug ? "selected" : ""; ?>
									<?php $name = $tag->name; ?>

									<option value="<?php echo $slug; ?>" <?php echo $selected; ?>>
										<?php echo $name; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</td>
			        </tr>
					<tr valign="top">
			        	<th scope="row">Tag 2 especial en el home</th>
			        	<td>
							<select 
								name="<?php echo self::TAG_SPECIAL_KEY_2; ?>" 
								id="<?php echo self::TAG_SPECIAL_KEY_2; ?>" 
								class="regular-text">

								<option value="" <?php echo $tag_2 == "" ? "selected" : "" ?>>Selectione un tag</option>
								<?php foreach($tags_list as $tag): ?>
									<?php $slug = $tag->slug; ?>
									<?php $selected = $tag_2 == $tag->slug ? "selected" : ""; ?>
									<?php $name = $tag->name; ?>

									<option value="<?php echo $slug; ?>" <?php echo $selected; ?>>
										<?php echo $name; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</td>
			        </tr>
			    </table>

			    <?php submit_button(); ?>
			</form>
		</div>
	<?php
	}

	public function register_options()
	{
		register_setting('home-widget-layout-custom-group', self::NUMBER_POST_KEY);
		register_setting('home-widget-layout-custom-group', self::TAG_SPECIAL_KEY_1);
		register_setting('home-widget-layout-custom-group', self::TAG_SPECIAL_KEY_2);
    }
    
    public static function get_number_post() {
        $option_value = intval(esc_attr(get_option(self::NUMBER_POST_KEY)));
        return $option_value == 0 ? 4 : $option_value;
	}
	
	public static function get_special_tag($post_ID, $option_tags) {

		$post_tags_list = wp_get_post_tags($post_ID);

		foreach($post_tags_list as $post_tag) {
			if(in_array($post_tag->slug, $option_tags)) {
				return $post_tag;
			}
		}
		
		return null;
	}

	public static function get_options_tags() {
		$tag_1 = esc_attr(get_option(self::TAG_SPECIAL_KEY_1));
		$tag_2 = esc_attr(get_option(self::TAG_SPECIAL_KEY_2));
		return array($tag_1, $tag_2);	
	}



	public function change_title_row_admin() {
		add_action("admin_head-edit.php", array($this, "wpse152971_edit_post_change_title_in_list"));
	}

	public function wpse152971_edit_post_change_title_in_list() {
		add_filter("the_title", array($this, "wpse152971_construct_new_title"), 100, 2);
	}
	
	public function wpse152971_construct_new_title($title, $id) {
		global $prefix;
		$position_key = $prefix . "position-layout";

		$position_value = get_post_meta($id, $position_key, true);
		
		if($position_value != "position-0" && $position_value != "") {
			$explode = explode("-", $position_value);
			if(is_array($explode) && isset($explode[1])) {
				$position_number = $explode[1];
				return $title . " — " . "Entrada Home #$position_number";
			}
			return $title;
		} 
		return $title;
	}


}

