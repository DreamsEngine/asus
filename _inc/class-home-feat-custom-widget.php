<?php


class HomeFeatCustomRegisterWidget {

    public function initialize() {
        add_action('widgets_init', array($this, 'register'));
    }

    function register() {
        register_widget('HomeFeatCustomWidget');
    }
}

class HomeFeatCustomWidget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'pinno_home_feat_custom_widget',
            'description' => 'A widget that displays the destacate posts',
        );
        parent::__construct( 'HomePinoWidget', ' ✳️ IT00 - Home Feat Widget Custom', $widget_ops);
    }

    public function widget($args, $instance) {
        include(plugin_dir_path(__FILE__) . 'views/widget-home-feat-custom-site-template.php');
    }

}
