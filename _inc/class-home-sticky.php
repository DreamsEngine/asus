<?php

class HomeSticky {

    public function initialize() {
        add_action('updated_post_meta', array($this, 'hook_post_meta'), 10, 4);
        add_action('added_post_meta', array($this, 'hook_post_meta'), 10, 4);
    }

    function hook_post_meta($meta_id, $post_id, $meta_key, $meta_value)
    {
        global $prefix;
        $key_position_layout = $prefix . 'position-layout';

        if ($meta_key === $prefix . 'home-layout' && $meta_value !== 'front-0') {
            $this->remove_sticky_post($post_id);
        } else if ($meta_key ===  $key_position_layout && $meta_value === 'position-1') {
            $post_list = self::get_post_by_position('position-1');
            $this->delete_all_positions($post_id, $post_list, $key_position_layout);
        } else if ($meta_key ===  $key_position_layout && $meta_value === 'position-2') {
            $post_list = self::get_post_by_position('position-2');
            $this->delete_all_positions($post_id, $post_list, $key_position_layout);
        } else if ($meta_key ===  $key_position_layout && $meta_value === 'position-3') {
            $post_list = self::get_post_by_position('position-3');
            $this->delete_all_positions($post_id, $post_list, $key_position_layout);
        } else if ($meta_key ===  $key_position_layout && $meta_value === 'position-4') {
            $post_list = self::get_post_by_position('position-4');
            $this->delete_all_positions($post_id, $post_list, $key_position_layout);
        } else if ($meta_key ===  $key_position_layout && $meta_value === 'position-5') {
            $post_list = self::get_post_by_position('position-5');
            $this->delete_all_positions($post_id, $post_list, $key_position_layout);
        }
    }

    function remove_sticky_post($post_id)
    {
        global $prefix;

        $stickies = get_option('sticky_posts');
        $stickies = array_diff($stickies, array($post_id));

        if (is_array($stickies)):
            foreach ($stickies as $stiky_post_id) {
                unstick_post($stiky_post_id);
                delete_post_meta($stiky_post_id, $prefix . 'home-layout');
            }
        endif;
    }

    public static function get_post_by_position($value) {

        global $prefix;

        $args = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,

            'posts_per_page' => -1,
            'post_type' => 'post',
            'meta_key' => $prefix . 'position-layout',
            'meta_value' => $value
        );

        $posts_list = get_posts($args);
        return $posts_list;
    }

    private function delete_all_positions($post_id, $list, $meta_key) {
        foreach ($list as $post_item) {
            if($post_item->ID !== $post_id) {
                delete_post_meta($post_item->ID, $meta_key);
            }   
        }
    }
}