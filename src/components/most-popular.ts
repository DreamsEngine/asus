import { html, LitElement, customElement, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';
import { Post } from "../shared/post";
import { fetchMostPopular } from "../shared/api";

@customElement('laoctava-most-popular')
export default class MostPopular extends LitElement {

    @property({ type: Array })
    postsList: Post[] = [];

    createRenderRoot() {
        return this;
    }

    async firstUpdated() {
        let postsList = await fetchMostPopular();
        this.postsList = postsList;
    }

    render() {
        return html`
            ${this.postsList.map(post => html`
                    <article class="la8_module--two__aside__article">
                       <figure>
                            <a href="${post.link}" title="${post.title}">
                                <img width="1200" height="675" alt="${post.title}" data-object-fit="cover" class="la8_module-image--full wp-post-image ls-is-cached" src="${post.featureImage}">
                            </a>
                       </figure>
                       <div class="la8_module--two__aside__article__metas">
                           <h6 class="la8_module--two__aside__article__title la8_title">
                               <a href="${post.link}" title="${post.title}">
                                   ${unsafeHTML(post.title)}
                               </a>
                           </h6>
                           <time class="la8_module--two__aside__article__time">${post.date}</time>
                       </div>
                   </article>
            `)}
        `;
    }

}
