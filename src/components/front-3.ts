import { html, LitElement, customElement, property } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';


@customElement('laoctava-front-3')
export default class HomeFrontLayout3 extends LitElement {

    @property({ type: String })
    iframe: String = '';

    @property({ type: String })
    logo: String = '';
    
    @property({ type: String })
    link: String = '';

    @property({ type: String })
    thumbnail: String = '';

    @property({ type: String })
    iconplay: String = '';

    @property({ type: Boolean })
    isShowVideo: boolean = false;
    

    createRenderRoot() {
        return this;
    }

    render() {
        return html`
            <article class="front-3__article">
                ${this.isShowVideo ? this.renderIframe() : this.renderThumbnail() }
                <aside class="front-3__article__meta">
                    <figure class="front-3__article__logo">
                        <a href="${this.link}" target="_blank">
                            <img src="${this.logo}" />
                        </a>
                    </figure>
                </aside>
            </article>
        `;
    }

    renderThumbnail() {
        return html`
            <figure class="front-3__article__figure">
                <img src="${this.thumbnail}" />
                <div class="front-3__article__figure__cover">
                    <img src="${this.iconplay}" width="50" @click="${this.handleOnChangePlay}" />
                </div>
            </figure>
        `;
    }

    renderIframe() {
        return unsafeHTML(this.iframe);
    }

    handleOnChangePlay() {
        this.isShowVideo =  !this.isShowVideo
    }

}
