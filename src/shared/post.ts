export interface Post {
    id: number;
    link: string;
    title: string;
    date: string;
    featureImage: string;
}
