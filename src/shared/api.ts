import { BASE_URL } from "./constants";
import { Post } from "./post";

export const fetchMostPopular = async (): Promise<Post[]> => {
    const response = await fetch(BASE_URL);
    return await response.json() as Post[];
};