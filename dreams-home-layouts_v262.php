<?php

/**
 * Plugin Name: Dreams Home Layouts
 * Plugin URI: https://dreamsengine.io/
 * Description: Tipos de layouts para el home mediante widgets.
 * Version: 2.6.2
 * Author: Dreams Engine
 * Author URI: https://dreamsengine.io/
 **/


if (!defined( 'WPINC' )) {
    die;
}

require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-home-metabox.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-home-enqueue.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/models/class-home-layouts-model.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-home-query-posts.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-home-layouts-widget.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-home-feat-custom-widget.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-widget-page-config.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-home-sticky.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-home-layouts-list-home.php');
require_once(plugin_dir_path( __FILE__ ) . '/_inc/class-widget-ajax-enpoints.php');

function home_layouts_start() {

    $home_layouts_widget = new  HomeLayoutsRegisterWidget();
    $home_feat_custom_widget = new HomeFeatCustomRegisterWidget();
    $home_metabox = new HomeMetabox();
    $home_enqueue = new HomeEnqueue();
    $home_sticky = new HomeSticky();
    $home_config = new HomeWidgetLayoutsConfig();
    $home_list_layouts = new HomeLayoutsListHome();
    $home_ajax_endpoints = new HomeWidgetAjaxEnpoints();

    if(is_admin()) {
        $home_enqueue->initialize_enqueue_scripts_admin();
    }

    $home_layouts_widget->initialize();
    $home_feat_custom_widget->initialize();
    $home_metabox->initialize();
    $home_sticky->initialize();
    $home_config->initialize();
    $home_config->change_title_row_admin();
    $home_enqueue->initialize_enqueue_scripts_site();
    $home_list_layouts->initialize();
    $home_ajax_endpoints->initialize();
}


home_layouts_start();